package org.firstinspires.ftc.teamcode.Vision;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.Button;
import org.firstinspires.ftc.teamcode.Vision.PipeLines.Compare;
import org.openftc.easyopencv.OpenCvCameraRotation;
import org.openftc.easyopencv.OpenCvInternalCamera;

@TeleOp(name="vision calibration")
public class Calibration extends OpMode {

    OpenCvInternalCamera camera;
    Compare pipeline;
    int dot = 0;
    double y = 400;
    Button cycle;

    @Override
    public void init() {
        pipeline = new Compare();
        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        camera = new OpenCvInternalCamera(OpenCvInternalCamera.CameraDirection.BACK, cameraMonitorViewId);
        camera.setPipeline(pipeline);
        camera.openCameraDevice();
        camera.startStreaming(320, 240, OpenCvCameraRotation.SIDEWAYS_RIGHT);
        cycle = new Button();
    }

    @Override
    public void loop() {
        dot = dot % 3;
        dot += cycle.get(gamepad1.a) ? 1 : 0;


        y += gamepad1.right_stick_y * 2;

        pipeline.cy0 = y;
        pipeline.cy1 = y;
        pipeline.cy2 = y;

        switch(dot){
            case 0:
                pipeline.cx0 += gamepad1.left_stick_x * 2;
                break;
            case 1:
                pipeline.cx1 += gamepad1.left_stick_x * 2;
                break;
            case 2:
                pipeline.cx2 += gamepad1.left_stick_x * 2;
                break;

        }

        telemetry.addData("0", "(" + pipeline.cx0 + ", " + pipeline.cy0 + "), " + pipeline.getVal0());
        telemetry.addData("1", "(" + pipeline.cx1 + ", " + pipeline.cy1 + "), " + pipeline.getVal1());
        telemetry.addData("2", "(" + pipeline.cx2 + ", " + pipeline.cy2 + "), " + pipeline.getVal2());
        telemetry.addData("controlling", dot);
        telemetry.addData("position", pipeline.getLocation());
        telemetry.addData("a", gamepad1.a);
    }
}
