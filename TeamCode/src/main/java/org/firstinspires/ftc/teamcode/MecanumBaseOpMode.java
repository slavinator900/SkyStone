package org.firstinspires.ftc.teamcode;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Position;
import org.firstinspires.ftc.robotcore.external.navigation.Velocity;

import org.firstinspires.ftc.teamcode.Vision.PipeLines.Compare;
import org.openftc.easyopencv.OpenCvCameraRotation;
import org.openftc.easyopencv.OpenCvInternalCamera;

import Utils.*;

import static com.qualcomm.robotcore.hardware.DcMotor.RunMode.RUN_TO_POSITION;
import static com.qualcomm.robotcore.hardware.DcMotor.RunMode.RUN_USING_ENCODER;

public abstract class MecanumBaseOpMode extends LinearOpMode{

    protected DcMotor motorLeftFront;
    protected DcMotor motorLeftBack;
    protected DcMotor motorRightFront;
    protected DcMotor motorRightBack;
    protected DcMotor rightIntake;
    protected DcMotor vertLift;
    protected DcMotor leftIntake;
    protected DcMotor horLift;
    protected Servo   arm;
    protected Servo clamp;
    protected OpenCvInternalCamera camera;
    protected Compare comp;

    BNO055IMU imu;
    public int ENCODER_THRESHOLD = 5;

    public boolean toggle;

    public double moveSpeedMin = .1;
    double moveSpeedMax = .4;
    public double ticksRatioForward = 42.8; //Ticks / Inch
    public double ticksRatioStrafe = 53.127;
    public double ticksRatioDiagonal = 80;

    public enum Direction {
        FORWARD, BACKWARD, LEFT, RIGHT;
    }

    public enum OpModeType {
        AUTONOMOUS, TELEOP;
    }

    public enum Diagonal {
        FR, FL, BR, BL
    }

    public enum StonePos{
        LEFT,RIGHT,CENTER
    }

    public enum BlockDir{
        IN,OUT
    }




    public double previousHeading = 0; //Saved heading from previous check, used to calculate integratedHeading
    public double integratedHeading = 0; //Saves heading in range (-inf, +inf) instead of (-179, 179)
    double turnSpeedMin = .25;

    public StonePos pos;



    public void mapHardware() {
        telemetry.addData("Init status","Mapping Hardware");
        telemetry.update();

        motorLeftFront = hardwareMap.dcMotor.get("fl");
        motorLeftBack = hardwareMap.dcMotor.get("bl");
        motorRightFront = hardwareMap.dcMotor.get("fr");
        motorRightBack = hardwareMap.dcMotor.get("br");
        rightIntake = hardwareMap.dcMotor.get("ri");
        vertLift = hardwareMap.dcMotor.get("ve");
        horLift = hardwareMap.dcMotor.get("he");
        leftIntake = hardwareMap.dcMotor.get("li");

        imu = hardwareMap.get(BNO055IMU.class, "imu 1");
        arm = hardwareMap.servo.get("arm");
        clamp = hardwareMap.servo.get("rin");



        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.calibrationDataFile = "BNO055IMUCalibration.json";
        parameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();
        parameters.mode = BNO055IMU.SensorMode.IMU;

        imu.initialize(parameters);
        imu.startAccelerationIntegration(new Position(), new Velocity(), 1000);

        comp = new Compare();


    }

    public void runOpMode(OpModeType type, Compare.Color color) {

        telemetry.addData("init status","runnnig opmode");
        telemetry.update();

        mapHardware();
        telemetry.addData("init stauts","Configuring motors");
        telemetry.update();
        motorLeftFront.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        motorLeftBack.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        motorRightFront.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        motorRightBack.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        vertLift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        horLift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        motorLeftFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorLeftBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        vertLift.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        horLift.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        motorLeftFront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorLeftBack.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorRightFront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorRightBack.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        vertLift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        horLift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        motorRightBack.setDirection(DcMotorSimple.Direction.REVERSE);
        motorRightFront.setDirection(DcMotorSimple.Direction.REVERSE);
        motorLeftBack.setDirection(DcMotorSimple.Direction.FORWARD);
        motorLeftFront.setDirection(DcMotorSimple.Direction.FORWARD);



        if (type == OpModeType.AUTONOMOUS){
            double startTime = getRuntime();

            int count =0;
            double lasttime=0;

            comp.color = color;
            int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
            camera = new OpenCvInternalCamera(OpenCvInternalCamera.CameraDirection.BACK, cameraMonitorViewId);
            camera.setPipeline(comp);
            camera.openCameraDevice();
            camera.startStreaming(320, 240, OpenCvCameraRotation.SIDEWAYS_RIGHT);

            while (!opModeIsActive() && !isStopRequested()){
                if (getRuntime()-lasttime > 1.7) {
                    lasttime=getRuntime();


                    pos = comp.getLocation();

                    telemetry.addData("Current Skystone Location: ",pos);
                    telemetry.update();


                    telemetry.addData("UPDATING:", count);
                    telemetry.update();
                    count++;


                }
            }
        }

        }

    public void runOpMode(OpModeType type) {

        telemetry.addData("init status","runnnig opmode");
        telemetry.update();

        mapHardware();
        telemetry.addData("init stauts","Configuring motors");
        telemetry.update();
        motorLeftFront.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        motorLeftBack.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        motorRightFront.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        motorRightBack.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        vertLift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        horLift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        motorLeftFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorLeftBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        vertLift.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        horLift.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        motorLeftFront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorLeftBack.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorRightFront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorRightBack.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        vertLift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        horLift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        motorRightBack.setDirection(DcMotorSimple.Direction.REVERSE);
        motorRightFront.setDirection(DcMotorSimple.Direction.REVERSE);
        motorLeftBack.setDirection(DcMotorSimple.Direction.FORWARD);
        motorLeftFront.setDirection(DcMotorSimple.Direction.FORWARD);



        if (type == OpModeType.AUTONOMOUS){
            double startTime = getRuntime();

            int count =0;
            double lasttime=0;

            while (!opModeIsActive() && !isStopRequested()){
                if (getRuntime()-lasttime > 1.7) {
                    lasttime=getRuntime();


                    pos = comp.getLocation();

                    telemetry.addData("Current Skystone Location: ",pos);
                    telemetry.update();


                    telemetry.addData("UPDATING:", count);
                    telemetry.update();
                    count++;


                }
            }
        }

    }

    public void turn(double degrees, Direction direction, double maxSpeed) {
        if (!opModeIsActive()) return;
        turn(degrees, direction, maxSpeed, 1, 10000);
    }

    public void turn(double degrees, Direction direction, double maxSpeed, int count, double timeout) {
        if (!opModeIsActive()) return; //Necessary because turn method is recursive
        if (direction.equals(Direction.RIGHT)) degrees *= -1; //Negative degree for turning left
        double initialHeading = getIntegratedHeading();
        double targetHeading = initialHeading + degrees; //Turns are relative to current position

        //Change mode because turn() uses motor power PID and not motor position
        motorLeftFront.setMode(RUN_USING_ENCODER);
        motorLeftBack.setMode(RUN_USING_ENCODER);
        motorRightFront.setMode(RUN_USING_ENCODER);
        motorRightBack.setMode(RUN_USING_ENCODER);

        ElapsedTime timer = new ElapsedTime();
        timer.startTime();

        //While target has not been reached, stops robot if target is overshot
        while (((degrees < 0 && getIntegratedHeading() > targetHeading) || (degrees > 0 && getIntegratedHeading() < targetHeading)) && (timer.milliseconds() < timeout) && opModeIsActive()) {
            double currentHeading = getIntegratedHeading();

            double robotSpeed = Range.clip(maxSpeed * (Math.abs(targetHeading - getIntegratedHeading()) / Math.abs(degrees)), turnSpeedMin, maxSpeed);

            motorLeftFront.setPower(degrees > 0 ? robotSpeed : -robotSpeed);
            motorLeftBack.setPower(degrees > 0 ? robotSpeed : -robotSpeed);
            motorRightFront.setPower(degrees > 0 ? -robotSpeed : robotSpeed);
            motorRightBack.setPower(degrees > 0 ? -robotSpeed : robotSpeed);

            telemetry.addData("Heading", getIntegratedHeading());
            telemetry.addData("Distance to turn: ", Math.abs(currentHeading - targetHeading));
            telemetry.addData("Target", targetHeading);
            telemetry.addData("Heading", currentHeading);
            telemetry.addData("Initial Heading", initialHeading);
            telemetry.addData("Power", robotSpeed);
            telemetry.update();
        }

        motorLeftFront.setPower(0);
        motorLeftBack.setPower(0);
        motorRightFront.setPower(0);
        motorRightBack.setPower(0);
        sleep(300);

        telemetry.addData("Distance to turn", Math.abs(getIntegratedHeading() - targetHeading));
        telemetry.addData("Direction", -1 * (int) Math.signum(degrees));
        telemetry.update();

        if (Math.abs(getIntegratedHeading() - targetHeading) > 1 && count > 0) { //If the target was significantly overshot
            //Recurse to correct turn, turning in the opposite direction
            turn(Math.abs(getIntegratedHeading() - targetHeading), direction.equals(Direction.LEFT) ? Direction.RIGHT : Direction.LEFT, .05, --count, 2000);
        }
    }

    public void move(double distance, Direction direction, double maxSpeed, boolean recurse, double timeout) {

        double initialHeading = getIntegratedHeading();
        double motorInitial = motorLeftFront.getCurrentPosition();



        if (direction == Direction.BACKWARD) {
            distance *= ticksRatioForward;
            motorLeftFront.setTargetPosition((int) (motorLeftFront.getCurrentPosition() + distance));
            motorLeftBack.setTargetPosition((int) (motorLeftBack.getCurrentPosition() + distance));
            motorRightFront.setTargetPosition((int) (motorRightFront.getCurrentPosition() + distance));
            motorRightBack.setTargetPosition((int) (motorRightBack.getCurrentPosition() + distance));
        } else if (direction == Direction.FORWARD) {
            distance *= ticksRatioForward;
            motorLeftFront.setTargetPosition((int) (motorLeftFront.getCurrentPosition() - distance));
            motorLeftBack.setTargetPosition((int) (motorLeftBack.getCurrentPosition() - distance));
            motorRightFront.setTargetPosition((int) (motorRightFront.getCurrentPosition() - distance));
            motorRightBack.setTargetPosition((int) (motorRightBack.getCurrentPosition() - distance));
        } else if (direction == Direction.LEFT) {
            distance *= ticksRatioStrafe;
            motorLeftFront.setTargetPosition((int) (motorLeftFront.getCurrentPosition() + distance));
            motorLeftBack.setTargetPosition((int) (motorLeftBack.getCurrentPosition() - distance));
            motorRightFront.setTargetPosition((int) (motorRightFront.getCurrentPosition() - distance));
            motorRightBack.setTargetPosition((int) (motorRightBack.getCurrentPosition() + distance));
        } else {
            distance *= ticksRatioStrafe;
            motorLeftFront.setTargetPosition((int) (motorLeftFront.getCurrentPosition() - distance));
            motorLeftBack.setTargetPosition((int) (motorLeftBack.getCurrentPosition() + distance));
            motorRightFront.setTargetPosition((int) (motorRightFront.getCurrentPosition() + distance));
            motorRightBack.setTargetPosition((int) (motorRightBack.getCurrentPosition() - distance));
        }
        //Change mode because move() uses setTargetPosition()
        motorLeftFront.setMode(RUN_TO_POSITION);
        motorLeftBack.setMode(RUN_TO_POSITION);
        motorRightFront.setMode(RUN_TO_POSITION);
        motorRightBack.setMode(RUN_TO_POSITION);

        double moveSpeed = moveSpeedMax;

        motorLeftFront.setPower(moveSpeed);
        motorLeftBack.setPower(moveSpeed);
        motorRightFront.setPower(moveSpeed);
        motorRightBack.setPower(moveSpeed);

        ElapsedTime timer = new ElapsedTime();
        timer.startTime();

        while ((motorLeftFront.isBusy() && motorLeftBack.isBusy() && motorRightFront.isBusy() && motorRightBack.isBusy()) && timer.milliseconds() < timeout && opModeIsActive()) {
            //Only one encoder target must be reached

            //Trapezoidal motion profile
/*
            if (Math.abs(motorLeftFront.getCurrentPosition() - motorInitial) < 500) {
                moveSpeed = Math.min(maxSpeed, moveSpeed + .03); //Ramp up motor speed at beginning of move
            } else if (Math.abs(motorLeftFront.getCurrentPosition() - motorLeftFront.getTargetPosition()) < 1000) { //Ramp down motor speed at end of move
                moveSpeed = Math.max(moveSpeedMin, moveSpeed - .04);
            }
*/
            //if(direction.equals(Direction.LEFT) || direction.equals(Direction.RIGHT)) moveSpeed = .8;
/*
            if (Math.abs(getIntegratedHeading() - initialHeading) > 2) {
                if (getIntegratedHeading() < initialHeading) { //Turn left
                    motorLeftFront.setPower(moveSpeed - .05);
                    motorLeftBack.setPower(moveSpeed - .05);
                    motorRightFront.setPower(moveSpeed + .05);
                    motorRightBack.setPower(moveSpeed + .05);
                } else { //Turn right
                    motorLeftFront.setPower(moveSpeed + .05);
                    motorLeftBack.setPower(moveSpeed + .05);
                    motorRightFront.setPower(moveSpeed - .05);
                    motorRightBack.setPower(moveSpeed - .05);
                }
            } else {
                motorLeftFront.setPower(moveSpeed);
                motorLeftBack.setPower(moveSpeed);
                motorRightFront.setPower(moveSpeed);
                motorRightBack.setPower(moveSpeed);
            }
*/

        }

        motorLeftFront.setPower(0);
        motorLeftBack.setPower(0);
        motorRightFront.setPower(0);
        motorRightBack.setPower(0);
        sleep(400);

        //Correct if robot turned during movement
        if (Math.abs(getIntegratedHeading() - initialHeading) > 2 && recurse) {

            turn(Math.abs(getIntegratedHeading() - initialHeading), getIntegratedHeading() < initialHeading ? Direction.LEFT : Direction.RIGHT, .05);
        }

        motorLeftFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorLeftBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }

    public void move45(double distance, Diagonal diagonal, double maxSpeed) {
        move45(distance, diagonal, maxSpeed, false);

    }

    public void move45(double distance, Diagonal diagonal, double maxSpeed, boolean recurse) {
        double initialHeading = getIntegratedHeading();

        //Change mode because move() uses setTargetPosition()
        motorLeftFront.setMode(RUN_TO_POSITION);
        motorLeftBack.setMode(RUN_TO_POSITION);
        motorRightFront.setMode(RUN_TO_POSITION);
        motorRightBack.setMode(RUN_TO_POSITION);

        double moveSpeed = maxSpeed;

        distance *= ticksRatioDiagonal;
        if (diagonal == Diagonal.BL) {
            motorLeftFront.setTargetPosition((int) (motorRightFront.getCurrentPosition() - distance));
            motorRightBack.setTargetPosition((int) (motorRightBack.getCurrentPosition() - distance));
            motorLeftFront.setPower(moveSpeed);
            motorLeftBack.setPower(0);
            motorRightFront.setPower(0);
            motorRightBack.setPower(moveSpeed);
            while (motorLeftFront.isBusy() && motorRightBack.isBusy()) {

            }
        } else if (diagonal == Diagonal.BR) {
            motorLeftBack.setTargetPosition((int) (motorRightFront.getCurrentPosition() - distance));
            motorRightFront.setTargetPosition((int) (motorRightBack.getCurrentPosition() - distance));
            motorLeftFront.setPower(0);
            motorLeftBack.setPower(moveSpeed);
            motorRightFront.setPower(moveSpeed);
            motorRightBack.setPower(0);
            while (motorLeftBack.isBusy() && motorRightFront.isBusy()) {

            }
        } else if (diagonal == Diagonal.FR) {
            motorRightFront.setTargetPosition((int) (motorRightFront.getCurrentPosition() + distance));
            motorLeftBack.setTargetPosition((int) (motorRightBack.getCurrentPosition() + distance));
            motorLeftFront.setPower(0);
            motorLeftBack.setPower(moveSpeed);
            motorRightFront.setPower(moveSpeed);
            motorRightBack.setPower(0);
            while (motorLeftBack.isBusy() && motorRightFront.isBusy()) {

            }
        } else if (diagonal == Diagonal.FL) {
            motorLeftFront.setTargetPosition((int) (motorRightFront.getCurrentPosition() + distance));
            motorRightBack.setTargetPosition((int) (motorRightBack.getCurrentPosition() + distance));
            motorLeftFront.setPower(moveSpeed);
            motorLeftBack.setPower(0);
            motorRightFront.setPower(0);
            motorRightBack.setPower(moveSpeed);
            while (motorLeftFront.isBusy() && motorRightBack.isBusy()) {

            }
        }

        //Correct if robot turned during movement
        if (Math.abs(getIntegratedHeading() - initialHeading) > 1 && recurse) {
            turn(Math.abs(getIntegratedHeading() - initialHeading), getIntegratedHeading() < initialHeading ? Direction.LEFT : Direction.RIGHT, .1);
        }

        motorLeftBack.setPower(0);
        motorRightFront.setPower(0);
        motorLeftFront.setPower(0);
        motorRightBack.setPower(0);
        sleep(300);


    }

    public double getIntegratedHeading() {
        //IMU is mounted vertically, so the Y axis is used for turning
        double currentHeading = imu.getAngularOrientation(AxesReference.EXTRINSIC, AxesOrder.XYZ, AngleUnit.DEGREES).thirdAngle;
        double deltaHeading = currentHeading - previousHeading;

        if (deltaHeading < -180) deltaHeading += 360;
        else if (deltaHeading >= 180) deltaHeading -= 360;

        integratedHeading += deltaHeading;
        previousHeading = currentHeading;

        return integratedHeading;
    }

    public void runIntake(BlockDir dir, int timeInSec){
        if(dir == BlockDir.IN) {
            new Thread(() -> {
                leftIntake.setPower(1);
                rightIntake.setPower(-1);
                sleep(timeInSec * 1000);
                leftIntake.setPower(0);
                rightIntake.setPower(0);
            }).start();
        }else{
            new Thread(() -> {
                leftIntake.setPower(-1);
                rightIntake.setPower(1);
                sleep(timeInSec * 1000);
                leftIntake.setPower(0);
                rightIntake.setPower(0);
            }).start();
        }
    }

//    Standard drive method
    public void drive(double leftStickX, double leftStickY, double rightStickX) {

        final double direction = Math.atan2(leftStickY, leftStickX);
        final double speed = RMath.dist(leftStickX, leftStickY);

        final double fl = speed * Math.sin(direction - Math.PI / 4.0) + rightStickX;
        final double fr = speed * Math.cos(direction - Math.PI / 4.0) - rightStickX;
        final double bl = speed * Math.cos(direction - Math.PI / 4.0) + rightStickX;
        final double br = speed * Math.sin(direction - Math.PI / 4.0) - rightStickX;

        motorLeftFront.setPower(-fl * moveSpeedMax);
        motorLeftBack.setPower(-bl * moveSpeedMax);
        motorRightFront.setPower(-fr * moveSpeedMax);
        motorRightBack.setPower(-br * moveSpeedMax);
    }


}
