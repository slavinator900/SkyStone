package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.MecanumBaseOpMode;
import org.firstinspires.ftc.teamcode.Vision.PipeLines.Compare;

@Autonomous(name = "Blue Side Depot", group = "neat")
public class BlueSideDepot extends MecanumBaseOpMode {

    public void runOpMode(){
        super.runOpMode(OpModeType.AUTONOMOUS, Compare.Color.BLUE);
        waitForStart();
        arm.setPosition(1);



        if(pos == StonePos.LEFT){
            move(3.5,Direction.BACKWARD,1,false,2000);
            move(27.5,Direction.LEFT,.3,false,5000);
            arm.setPosition(0);
            sleep(500);
            move(18,Direction.RIGHT,.2,false,5000);
            move(2,Direction.FORWARD,.05,false,5000);
            move(52,Direction.BACKWARD,.4,false,10000);
            arm.setPosition(1);
            sleep(500);
            move(76.5,Direction.FORWARD,1,false,5000);
            move(15,Direction.LEFT,.1,false,5000);
            arm.setPosition(0);
            sleep(500);
            move(19,Direction.RIGHT,.2,false,5000);
            move(2,Direction.FORWARD,.05,false,5000);
            move(68,Direction.BACKWARD,.4,false,10000);
            arm.setPosition(1);
            move(14,Direction.FORWARD,.1,false,10000);
        } else if (pos == StonePos.RIGHT){
            move(13.5,Direction.FORWARD,1,false,20000);
            move(27.5,Direction.LEFT,.3,false,5000);
            arm.setPosition(0);
            sleep(500);
            move(18,Direction.RIGHT,.2,false,5000);
            move(2,Direction.FORWARD,.05,false,5000);
            move(62,Direction.BACKWARD,.4,false,10000);
            arm.setPosition(1);
            sleep(500);
            new Thread(() -> {
                leftIntake.setPower(.5);
                sleep(2000);
                leftIntake.setPower(0);
            }).start();
            move(69,Direction.FORWARD,1,false,5000);
            move(23.5,Direction.LEFT,.1,false,5000);
            runIntake(BlockDir.IN,5);
            move(5,Direction.FORWARD,.5,false,5000);
            move(3,Direction.BACKWARD,.5,false,5000);
            move(20,Direction.RIGHT,.5,false,5000);
            move(75,Direction.BACKWARD,.6,false,10000);
            move(10,Direction.FORWARD,.6,false,5000);
            turn(180,Direction.LEFT,.5,0,5000);
            runIntake(BlockDir.OUT,7);
            move(15,Direction.BACKWARD,.5,false,5000);


        } else{
            move(5,Direction.FORWARD,.5,false,2000);
            move(27.5,Direction.LEFT,.3,false,5000);
            arm.setPosition(0);
            sleep(500);
            move(18,Direction.RIGHT,.2,false,5000);
            move(2,Direction.FORWARD,.05,false,5000);
            move(53.5,Direction.BACKWARD,.4,false,10000);
            arm.setPosition(1);
            sleep(500);
            move(77.5,Direction.FORWARD,1,false,5000);
            move(15,Direction.LEFT,.1,false,5000);
            arm.setPosition(0);
            sleep(500);
            move(19,Direction.RIGHT,.2,false,5000);
            move(2,Direction.FORWARD,.05,false,5000);
            move(78,Direction.BACKWARD,.4,false,10000);
            arm.setPosition(1);
            move(14,Direction.FORWARD,.1,false,10000);

        }

        move(12,Direction.LEFT,.01,false,100000000);
    }
}
