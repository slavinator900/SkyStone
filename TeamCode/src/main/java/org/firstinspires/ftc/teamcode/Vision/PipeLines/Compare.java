package org.firstinspires.ftc.teamcode.Vision.PipeLines;

import org.firstinspires.ftc.teamcode.MecanumBaseOpMode.StonePos;
import org.firstinspires.ftc.teamcode.Vision.Overlay;
import org.firstinspires.ftc.teamcode.Vision.Tracker;
import org.firstinspires.ftc.teamcode.Vision.VisionCamera;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.openftc.easyopencv.OpenCvCamera;
import org.openftc.easyopencv.OpenCvPipeline;

public class Compare extends OpenCvPipeline {
    private Mat mat0;
    private Mat mat1;
    private Mat mat2;

    private Mat mask0;
    private Mat mask1;
    private Mat mask2;

    private boolean madeMats = false;

    private Scalar BLACK = new Scalar(0,0,0);
    private Scalar WHITE = new Scalar(255,255,255);
    private Scalar RED = new Scalar(0, 0, 255);

    public double cx0;
    public double cy0;
    public double cx1;
    public double cy1;
    public double cx2;
    public double cy2;

    private int r = 10;
    private int strokeWidth = 3;

    private double val0 = 0;
    private double val1 = 0;
    private double val2 = 0;

    public Color color = Color.BLUE;

    private StonePos location = StonePos.RIGHT;

    public enum Color{
        RED,
        BLUE
    }

    public Mat processFrame(Mat frame) {

        switch(color){
            case RED:
                cx0 = 19.91;
                cy0 = 134.94;
                cx1 = 93.52;
                cy1 = 134.94;
                cx2 = 153.73;
                cy2 = 134.94;
                break;
            case BLUE:
                cx0 = 65.73;
                cy0 = 141.98;
                cx1 = 127.48;
                cy1 = 141.98;
                cx2 = 187.80;
                cy2 = 141.98;
                break;
        }

        int h = frame.height();
        int w = frame.width();

//        Creating the new mats on the first frame
        int type = frame.type();
        if (!madeMats) {
            mask0 = new Mat(h, w, type);
            mask1 = new Mat(h, w, type);
            mask2 = new Mat(h, w, type);
            mat0 = new Mat();
            mat1 = new Mat();
            mat2 = new Mat();
            madeMats = true;
        }

        mask0.setTo(BLACK);
        mask1.setTo(BLACK);
        mask2.setTo(BLACK);

//        Creating circles that go in the center of the stones
        Imgproc.circle(mask0, new Point(cx0, cy0), r, WHITE, Core.FILLED);
        Imgproc.circle(mask1, new Point(cx1, cy1), r, WHITE, Core.FILLED);
        Imgproc.circle(mask2, new Point(cx2, cy2), r, WHITE, Core.FILLED);

//        Capturing only the circles that are defined above
        Core.bitwise_and(mask0, frame, mat0);
        Core.bitwise_and(mask1, frame, mat1);
        Core.bitwise_and(mask2, frame, mat2);

        double[] rgb0 = Core.sumElems(mat0).val;
        val0 = rgb0[0] + rgb0[1] + rgb0[2];

        double[] rgb1 = Core.sumElems(mat1).val;
        val1 = rgb1[0] + rgb1[1] + rgb1[2];

        double[] rgb2 = Core.sumElems(mat2).val;
        val2 = rgb2[0] + rgb2[1] + rgb2[2];


        if (val0 < val1 && val0 < val2) {
            location = StonePos.LEFT;
        } else if (val1 < val0 && val1 < val2) {
            location = StonePos.CENTER;
        } else {
            location = StonePos.RIGHT;
        }

        Scalar s0 = WHITE;
        Scalar s1 = WHITE;
        Scalar s2 = WHITE;
        double r0 = r;
        double r1 = r;
        double r2 = r;

        if (location == StonePos.RIGHT) {
            s2 = RED;
            r2= r * 2;
        } else if (location == StonePos.LEFT) {
            s0 = RED;
            r0 = r * 2;
        } else {
            s1 = RED;
            r1 = r * 2;
        }

        Imgproc.line(frame, new Point(0, 275), new Point(300, 275), new Scalar(0, 255, 0));
        Imgproc.circle(frame, new Point(cx0, cy0), (int)r0, s0, Core.FILLED);
        Imgproc.circle(frame, new Point(cx1, cy1), (int)r1, s1, Core.FILLED);
        Imgproc.circle(frame, new Point(cx2, cy2), (int)r2, s2, Core.FILLED);



        return frame;
    }

    public double getVal0(){
        return val0;
    }
    public double getVal1(){
        return val1;
    }
    public double getVal2(){
        return val2;
    }



    public StonePos getLocation() {
        return location;
    }
}