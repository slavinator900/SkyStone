package org.firstinspires.ftc.teamcode.TeleOp;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.teamcode.Button;
import org.firstinspires.ftc.teamcode.MecanumBaseOpMode;
import org.firstinspires.ftc.teamcode.Vision.PipeLines.Compare;

import Utils.*;

@TeleOp(name = "TeleOp", group = "Nice")
public class MainOp extends MecanumBaseOpMode {

    double maxSpeed = 0.4;
    double initmaxspeed = 0.6;
    boolean turbo = false;
    double vertEx = 20;
    double horzEx = 5;
    int vertTicks = 300;
    int horzTicks = 300;

    @Override
    public void runOpMode() {
        super.runOpMode(OpModeType.TELEOP);

        Button clampPress = new Button();

        waitForStart();
        while (opModeIsActive()) {

            turbo = gamepad1.right_trigger > 0.2;

            double intake = 0;
            intake += (gamepad1.left_bumper? 1:0) - (gamepad1.right_bumper? 1:0);

            rightIntake.setPower(intake);
            leftIntake.setPower(-intake);

            clampPress.get(gamepad2.a);
            clamp.setPosition(clampPress.toggle()? 0 : 1);

//            Locking position if not powered
            if(horLift.getMode() == DcMotor.RunMode.RUN_USING_ENCODER && gamepad2.right_stick_y == 0){
                horLift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
                horLift.setTargetPosition(horLift.getCurrentPosition());
                horLift.setPower(1);
            } else if(horLift.getMode() == DcMotor.RunMode.RUN_TO_POSITION && gamepad2.right_stick_y != 0){
                horLift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            } else if(horLift.getMode() == DcMotor.RunMode.RUN_USING_ENCODER){
                double d = horLift.getCurrentPosition() + gamepad2.right_stick_y * 10;
                if(RMath.isWithin(d, 0, horzTicks))
                    horLift.setPower(gamepad2.right_stick_y);
            }

            if(vertLift.getMode() == DcMotor.RunMode.RUN_USING_ENCODER && gamepad2.left_stick_y == 0){
//                Locking motors
                vertLift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
                vertLift.setTargetPosition(vertLift.getCurrentPosition());
                vertLift.setPower(1);
            } else if(vertLift.getMode() == DcMotor.RunMode.RUN_TO_POSITION && gamepad2.left_stick_y != 0){
//                Running normally
                vertLift.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            } else if(vertLift.getMode() == DcMotor.RunMode.RUN_USING_ENCODER){
//                Setting powers
                double d = vertLift.getCurrentPosition() + gamepad2.left_stick_y * 10;
                if(RMath.isWithin(d, 0, vertTicks))
                    vertLift.setPower(gamepad2.left_stick_y);
            }

            telemetry.addData("hz", horLift.getCurrentPosition());
            telemetry.addData("vt", vertLift.getCurrentPosition());
            telemetry.update();

            final double hp = horLift.getCurrentPosition();
            final double vp = vertLift.getCurrentPosition();
            final double armDist = RMath.dist(hp / horzTicks * horzEx, vp / vertTicks * vertEx);
            final double maxDist = RMath.dist(vertEx, horzEx) + 1;
            final double coeff = 1 - (armDist / maxDist);
            drive(gamepad1.left_stick_x * coeff,
                    gamepad1.left_stick_y * coeff,
                    gamepad1.right_stick_x * coeff
            );

        }
    }
}